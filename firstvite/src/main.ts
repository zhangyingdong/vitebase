import { createApp } from 'vue'
import App from './App.vue'
import './index.css'

import router from './router/router'
import vuex from './store/index'
import { Button, message,Menu,Icon} from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

const app = createApp(App);
app.use(router)
app.use(vuex)
app.use(Button)
app.use(Menu)
app.use(Icon)
app.mount('#app')
