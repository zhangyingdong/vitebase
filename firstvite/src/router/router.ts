import { createRouter, createWebHashHistory } from "vue-router";

import home from '../views/home.vue'
import userInfo from '../views/userInfo.vue'

// 在 Vue-router新版本中，需要使用createRouter来创建路由
export default createRouter({
    // 指定路由的模式,此处使用的是hash模式
    history: createWebHashHistory(),

    // 路由地址
    routes: [
        { path: '/', component: home },
        { path: '/test', component: home },
        {
            path: '/home',
            name: 'Home',
            component: home,
            children: [{
                path: 'main',
                name: 'main',
                component: home
            },{
                path: 'userInfo',
                name: 'userInfo',
                component: userInfo
            }

            ]
        }

    ],
});
